// import thư viện mongose 
const mogoose = require("mongoose");
//improt Model 
const userModel = require("../model/userModel");
const diceHistoryModel = require("../model/diceHistoryModel");
const prizeHistoryModel = require("../model/prizeHistoryModel");
const prizeModel = require("../model/prizeModel");
const voucherHistoryModel = require("../model/voucherHistoryModel");
const voucherModel = require("../model/voucherModel");



//tạo ném dice 
const diceHandler = (req, res) => {
    // B1: Chuẩn bị dữ liệu
    let username = req.body.username;
    let firstname = req.body.firstname;
    let lastname = req.body.lastname;

    // Random 1 giá trị xúc xắc bất kỳ
    let dice = Math.floor(Math.random() * 6 + 1);

    // B2: Validate dữ liệu từ request body
    if (!username) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Username chưa chính xác"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Firstname chưa chính xác"
        })
    }

    if (!lastname) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Lastname chưa chính xác"
        })
    }
    // Sử dụng userModel tìm kiếm bằng username
    userModel.findOne({ username: username }, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            if (!user) { //nếu dữ liệu là null, hoặc undefine thì tạo mới một user
                //tạo mới user 
                userModel.create({
                    _id: mogoose.Types.ObjectId(),
                    username: username,
                    firstname: firstname,
                    lastname: lastname
                }, (errCreat, dataCreat) => {
                    if (errCreat) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreate.message
                        })
                    }
                    else {
                        // Xúc xắc 1 lần, lưu lịch sử vào Dice History
                        diceHistoryModel.create({
                            _id: mogoose.Types.ObjectId(),
                            user: dataCreat._id,
                            dice: dice
                        }, (errorDiceHistoryCreate, diceHistoryCreated) => {
                            if (errorDiceHistoryCreate) {
                                return res.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: errorDiceHistoryCreate.message
                                })
                            }
                            else {
                                if (dice < 3) {
                                    // Nếu dice < 3, không nhận được voucher và prize gì cả
                                    return res.status(200).json({
                                        dice: dice,
                                        prize: null,
                                        voucher: null
                                    })
                                }
                                else {
                                    // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                                    voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                        let randomVoucher = Math.floor(Math.random * countVoucher);
                                        voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                            // Lưu voucher History
                                            voucherHistoryModel.create({
                                                _id: mogoose.Types.ObjectId(),
                                                user: dataCreat._id,
                                                voucher: findVoucher._id
                                            }, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                                if (errorCreateVoucherHistory) {
                                                    return res.status(500).json({
                                                        status: "Error 500: Internal server error",
                                                        message: errorCreateVoucherHistory.message
                                                    })
                                                }
                                                else {
                                                    if (errorCreateVoucherHistory) {
                                                        return res.status(500).json({
                                                            status: "Error 500: Internal server error",
                                                            message: errorCreateVoucherHistory.message
                                                        })
                                                    }
                                                    else {
                                                        // User mới không có prize
                                                        return res.status(200).json({
                                                            dice: dice,
                                                            prize: null,
                                                            voucher: findVoucher
                                                        })
                                                    }
                                                }
                                            })
                                        })
                                    })
                                }
                            }
                        })
                    }

                })
            }
            else {
                //Nếu user đã tồn tại trong hệ thống
                // Xúc xắc 1 lần, lưu lịch sử vào Dice History
                diceHistoryModel.create({
                    _id: mogoose.Types.ObjectId(),
                    user: user._id,
                    dice: dice
                }, (errorDiceHistoryCreate, diceHistoryCreated) => {
                    if (errorDiceHistoryCreate) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errorDiceHistoryCreate.message
                        })
                    }
                    else {
                        if (dice < 3) {
                            // Nếu dice < 3, không nhận được voucher và prize gì cả
                            return res.status(200).json({
                                dice: dice,
                                prize: null,
                                voucher: null
                            })
                        }
                        else {
                            // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                            voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                let randomVoucher = Math.floor(Math.random * countVoucher);
                                voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                    // Lưu voucher History
                                    voucherHistoryModel.create({
                                        _id: mogoose.Types.ObjectId(),
                                        user: user._id,
                                        voucher: findVoucher._id
                                    }, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                        if (errorCreateVoucherHistory) {
                                            return res.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: errorCreateVoucherHistory.message
                                            })
                                        }
                                        else {
                                            // Lấy 3 lần gieo xúc xắc gần nhất của user
                                            diceHistoryModel
                                                .find()
                                                .sort({ _id: -1 })
                                                .limit(3)
                                                .exec((errorFindLast3DiceHistory, last3DiceHistory) => {
                                                    if (errorFindLast3DiceHistory) {
                                                        return res.status(500).json({
                                                            status: "Error 500: Internal server error",
                                                            message: errorFindLast3DiceHistory.message
                                                        })
                                                    }
                                                    else {
                                                        // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                        if (last3DiceHistory.length < 3) {
                                                            return res.status(200).json({
                                                                dice: dice,
                                                                prize: null,
                                                                voucher: findVoucher
                                                            })
                                                        }
                                                        else {
                                                            console.log(last3DiceHistory)
                                                            // Kiểm tra 3 dice gần nhất
                                                            let checkHavePrize = true;
                                                            last3DiceHistory.forEach(diceHistory => {
                                                                if (diceHistory.dice < 3) {
                                                                    // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được giải thưởng
                                                                    checkHavePrize = false;
                                                                }
                                                            });
                                                            if (!checkHavePrize) {
                                                                return res.status(200).json({
                                                                    dice: dice,
                                                                    prize: null,
                                                                    voucher: findVoucher
                                                                })
                                                            }
                                                            else {
                                                                // Nếu đủ điều kiện nhận giải thưởng, tiến hành lấy random 1 prize trong prize Model
                                                                prizeModel.count().exec((errorCountPrize, countPrize) => {
                                                                    let randomPrize = Math.floor(Math.random * countPrize);
                                                                    prizeModel.findOne().skip(randomPrize).exec((errorFindPrize, findPrize) => {
                                                                        prizeHistoryModel.create({
                                                                            _id: mogoose.Types.ObjectId(),
                                                                            user: user._id,
                                                                            prize: findPrize._id
                                                                        }, (errorCreatePrizeHistory, voucherPrizeCreated) => {
                                                                            if (errorCreatePrizeHistory) {
                                                                                return res.status(500).json({
                                                                                    status: "Error 500: Internal server error",
                                                                                    message: errorCreatePrizeHistory.message
                                                                                })
                                                                            }
                                                                            else {
                                                                                // Trả về kết quả cuối cùng
                                                                                return res.status(200).json({
                                                                                    dice: dice,
                                                                                    prize: findPrize,
                                                                                    voucher: findVoucher
                                                                                })
                                                                            }
                                                                        })
                                                                    })
                                                                })
                                                            }
                                                        }
                                                    }
                                                })
                                        }
                                    })
                                })
                            })
                        }
                    }

                })
            }
        }

    })
}
// LẤY DICE HISTORY BY USERNAME
const getDiceHistoryByUsername = (req, res) => {
    //B1: thu thập dữ liệu
    let username = req.query.username
    var condition = {}
    if (!username) {
        diceHistoryModel.find((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: err.message
                })
            }
            else {
                return res.status(200).json({
                    message: "lấy toàn bộ dữ liệu thành công nhé",
                    data: data
                })
            }
        })
    }
    else {
        userModel.findOne({ username: username }, (errUser, dataUser) => {
            if (errUser) {
                return res.status(500).json({
                    message: "không tìm thấy dữ liệu"
                })
            }
            else {
                if (!dataUser) {
                    return res.status(400).json({
                        message: "không tồn tại dữ liệu",
                        data: []
                    })
                }
                else {
                    if (dataUser) { condition.user = dataUser._id }
                    diceHistoryModel.find(condition).populate("user").exec((errDice, dataDice) => {
                        if (errDice) {
                            return res.status(500).json({
                                message: "username không tồn tại"
                            })
                        }
                        else {
                            return res.status(200).json({
                                message: "Thành công lấy dữ liệu hh",
                                data: dataDice
                            })
                        }
                    })
                }
            }
        })
    }
}
// LẤY VOUCHER BY USERNAME
const getVoucherByUsername = (req, res) => {
    //B1: thu thập dữ liệu
    let username = req.query.username
    var condition = {}
    if (!username) {
        voucherHistoryModel.find((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: err.message
                })
            }
            else {
                return res.status(200).json({
                    message: "lấy toàn bộ dữ liệu thành công nhé",
                    data: data
                })
            }
        })
    }
    else {
        userModel.findOne({ username: username }, (errUser, dataUsser) => { // nhớ là hàm findOne dùng hàm find thường là sai nhé
            if (errUser) { // nếu lỗi thì trả ra không tìm thấy dữ liệu
                return res.status(500).json({
                    message: "không tìm thấy dữ liệu"
                })
            }
            else {
                if (!dataUsser) {
                    return res.status(500).json({
                        message: "nhả ra dữ liệu là mảng rỗng",
                        data: []
                    })
                }
                else {// nếu có dữ liệu thì condition sẽ là điều kiện tìm điều kiện tìm là id của dữ liệu
                    if (dataUsser) {
                        condition.user = dataUsser._id
                    }
                    // tìm dữ liệu theo điều kiện liên quan tới user tìm dữ liệu theo điều kiện thì dùng hàm find()
                    voucherHistoryModel.find(condition)
                        .populate("user")
                        .populate("voucher")
                        .exec((errVoucher, dataVoucher) => {
                            if (errVoucher) {
                                return res.status(500).json({
                                    message: " Không có dữ liệu đâu haha"
                                })
                            }
                            else {
                                return res.status(200).json({
                                    message: "Thành công lấy dữ liệu nè",
                                    data: dataVoucher
                                })
                            }
                        })
                }
            }
        })
    }

}

//LẤY LỊCH SỬ QUÀ TẶNG THÔNG QUA USERNAME
const getPrizeHistory = (req, res) => {
    //B1: thu thập dữ liệu
    let username = req.query.username
    var condition = {}
    if (!username) {
        prizeHistoryModel.find((err, data) => { // hàm find là hàm tìm hết tất cả các thứ
            if (err) {
                return res.status(500).json({
                    message: " không tìm được dữ liệu đâu nhé"
                })
            }
            else {
                return res.status(200).json({
                    message: "Không tìm thấy username nên show ra toàn bộ lịch sử quà tặng",
                    data: data
                })
            }
        })
    }
    else { // nếu có user thì dùng model user tìm theo tên 
        userModel.findOne({ username: username }, (errUsser, dataUser) => {
            if (errUsser) { // nếu lỗi thì nhả ra không có dữ liệu
                return res.status(500).json({
                    message: " lỗi dữ liệu không tìm được dữ liệu đâu nhé"
                })
            }
            else { // còn không lỗi thì tìm tiếp
                if (!dataUser) { // nếu không tìm thấy dataUser trong dữ liệu
                    return res.status(500).json({
                        message: " không tìm được dữ liệu trong database"
                    })
                }
                else { // nếu tìm thấy thì gọi  prizeHistoryModel tìm theo điều kiện _id
                    if (dataUser) {
                        condition.user = dataUser._id
                        console.log(condition)
                    }
                    prizeHistoryModel.find(condition).populate("user").populate("prize").exec((errPrize, dataPrize) => {
                        if (errPrize) {
                            return res.status(500).json({
                                message: " Không có dữ liệu đâu haha"
                            })
                        }
                        else {
                            return res.status(200).json({
                                message: "Thành công lấy dữ liệu nè",
                                data: dataPrize
                            })
                        }
                    })
                }
            }
        })
    }
}



module.exports = {
    diceHandler,
    getDiceHistoryByUsername,
    getVoucherByUsername,
    getPrizeHistory
}
