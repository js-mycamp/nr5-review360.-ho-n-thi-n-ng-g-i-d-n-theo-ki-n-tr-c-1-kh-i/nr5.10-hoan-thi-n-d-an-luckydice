// import thư viện mongose 
const mongoose = require("mongoose");
//improt PrizeModel 
const prizeModel = require("../model/prizeModel");
// Tạo function createPrize
const createPrize = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
          message: 'dữ liệu Name không đúng'
        })
      }
      // B3 Gọi model thực hiện các thao tác nghiệp vụ
      let newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
      }
      prizeModel.create(newPrize,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                Prize: data
            })
        }
      })
};
// Tạo function getAllPrize
const getAllPrize =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 Prize: data
             })
         }
    })
};
// Tạo function getPrizeById
const getPrizeById =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.prizeId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.findById(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công thông qua Id",
                 Prize: data
             })
         }
    })
};
// Tạo function updatePrizeById
const updatePrizeById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id= req.params.prizeId;
    let body = req.body
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    if(!body.name){
        return res.status(400).json({
          message: 'dữ liệu Name không đúng'
        })
    };
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    // thu thập dữ liệu cập nhật
    let updateData ={
        name: body.name,
        description: body.description,
    }
     // cập nhật dữ liệu
     prizeModel.findByIdAndUpdate(id,updateData,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng  ID",
                 updateData: data
                })
            }
        })
    };
// Tạo function deletePrizeById
const deletePrizeById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id= req.params.prizeId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Thành công xoá dữ liệu thông qua ID",
                 DeleteData: data
             })
         }
    })
}
//exprot controller
module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}
