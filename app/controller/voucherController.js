// import thư viện mongose 
const mongoose = require("mongoose");
//improt VoucherModel
const voucherModel = require("../model/voucherModel");

// Tạo function createVoucher
const createVoucher =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2: validate dữ liệu
    if(!body.code) {
        return res.status(400).json({
          message: 'dữ liệu code không đúng'
        })
      }
      if(!Number.isInteger(body.discount) || body.discount < 0) {
        return res.status(400).json({
          message: 'Dữ liệu discount không đúng'
        })
      }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    //thu thập dữ liệu
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    //thực hiện tạo dữ liệu
    voucherModel.create(newVoucher,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới dữ liệu thành công",
                Voucher: data
            })
        }
    })
};
// Tạo function getAllVoucher 
const getAllVoucher = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 Voucher: data
             })
         }
    })
};
// Tạo function getVoucherById
const getVoucherById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findById(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công thông qua Id",
                 Voucher: data
             })
         }
    })
};
//Tạo function updateVoucherById
const updateVoucherById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherId;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    if(!body.code) {
        return res.status(400).json({
          message: 'dữ liệu code không đúng'
        })
      }
      if(!Number.isInteger(body.discount) || body.discount < 0) {
        return res.status(400).json({
          message: 'Dữ liệu discount không đúng'
        })
      }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    // thu thập dữ liệu
    let updateData = {
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    // thực hiện chỉnh sửa
    voucherModel.findByIdAndUpdate(id,updateData,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng ID",
                 updateData: data
             })
         }
    })
};
// Tạo function deleteVoucherById
const deleteUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Thành công xoá dữ liệu thông qua ID",
                 DeleteData: data
                })
            }
        })
};

//exprot controller
module.exports ={
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteUserById
}
