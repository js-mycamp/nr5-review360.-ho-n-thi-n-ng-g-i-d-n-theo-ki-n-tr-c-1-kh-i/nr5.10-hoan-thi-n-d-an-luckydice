//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const userController = require("../controller/userController");
//Tạo router
const userRouter = express.Router();

userRouter.post("/users",middlerware.Middeware,userController.createUser);
userRouter.get("/users",middlerware.Middeware,userController.getAllUser);
userRouter.get("/users/:userId",middlerware.Middeware,userController.getUserById);
userRouter.put("/users/:userId",middlerware.Middeware,userController.updateUserById);
userRouter.delete("/users/:userId",middlerware.Middeware,userController.deleteUserById);


// exprot router
module.exports = {userRouter};
