//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const VoucherHistoryMiddlerware= require("../middleware/middleware");
//tạo router
const VoucherHistoryRouter = express.Router();
//improt controller
const VoucherHistoryController = require("../Controller/voucherHistoryController");

VoucherHistoryRouter.post("/voucher-histories",VoucherHistoryMiddlerware.Middeware,VoucherHistoryController.createVoucherHistory);
VoucherHistoryRouter.get("/voucher-histories/",VoucherHistoryMiddlerware.Middeware,VoucherHistoryController.getAllVoucherHistory);
VoucherHistoryRouter.get("/voucher-histories/:historyId",VoucherHistoryMiddlerware.Middeware,VoucherHistoryController.getVoucherHistoryById);
VoucherHistoryRouter.put("/voucher-histories/:historyId",VoucherHistoryMiddlerware.Middeware,VoucherHistoryController.updateVoucherHistoryById);
VoucherHistoryRouter.delete("/voucher-histories/:historyId",VoucherHistoryMiddlerware.Middeware,VoucherHistoryController.deleteVoucherHistoryById);


// exprot router
module.exports = {VoucherHistoryRouter};
