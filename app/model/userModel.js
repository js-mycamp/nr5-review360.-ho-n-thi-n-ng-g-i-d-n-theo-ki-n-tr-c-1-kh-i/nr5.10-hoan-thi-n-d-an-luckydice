// khai báo thư viện mongoose
const mogoose = require("mongoose");
// khai báo class schema của mongo
const schema = mogoose.Schema;
//khai báo  review schema
const UserSchema = new schema ({
    username: {type:String, require: true, unique: true},
    firstname: {type:String, require: true},
    lastname: {type:String, require: true}
},{timestamps:true});

// exprot mongoose
module.exports = mogoose.model("User",UserSchema);
