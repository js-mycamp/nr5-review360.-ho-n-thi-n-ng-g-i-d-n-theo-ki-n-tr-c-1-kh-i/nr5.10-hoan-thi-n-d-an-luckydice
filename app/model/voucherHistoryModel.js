// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const VoucherHistorySchema = new schema ({
    user:{type: mongoose.Types.ObjectId, ref:"User", require: true},
    voucher:{type: mongoose.Types.ObjectId, ref:"Voucher", require: true}
},{timestamps:true});
// exprot mongoose
module.exports = mongoose.model("VoucherHistory",VoucherHistorySchema);