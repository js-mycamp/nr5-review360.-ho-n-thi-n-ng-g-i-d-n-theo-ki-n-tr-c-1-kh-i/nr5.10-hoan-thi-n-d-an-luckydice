// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const PrizeHistorySchema = new schema ({
    user:{type: mongoose.Types.ObjectId, ref:"User", require: true},
    prize:{type: mongoose.Types.ObjectId,ref:"Prize",require:true}
},{timestamps:true})
// exprot mongoose
module.exports = mongoose.model("PrizeHistory",PrizeHistorySchema);
