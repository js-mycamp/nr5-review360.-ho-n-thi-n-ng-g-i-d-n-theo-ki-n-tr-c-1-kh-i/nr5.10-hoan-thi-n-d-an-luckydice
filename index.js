// Khai bá thư viện express
const express = require("express");
//Khai báo thư viện mongosse
const mongoose = require("mongoose");
// Khai báo thư viện đường dẫn
const path = require("path");
const { diceRouter } = require("./app/router/dice.router");

//khai báo router
const { diceHistoryRouter } = require("./app/router/diceHistoryRouter");
const { prizeHistoryRouter } = require("./app/router/prizeHistoryRouter");
const { prizeRouter } = require("./app/router/prizeRouter");
const { userRouter } = require("./app/router/userRouter");
const {VoucherHistoryRouter} = require("./app/router/voucherHistoryRouter");
const { voucherRouter } = require("./app/router/voucherRouter");
//Tạo app express
const app = express();
//khai báo cổng chạy 
const port = 8000
//sử dụng được body json
app.use(express.json());
//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}));

//middleware static
app.use(express.static(__dirname + '/views'));
//Khai báo API
app.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/Task 23B.50web casion.html"))
})

// kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDiceCasino", (error) => {
  if (error) throw error;
  console.log("Connect to MongooseDB Successfully !");
})
//sử dụng app
app.use(userRouter);
app.use(diceHistoryRouter);
app.use(prizeRouter);
app.use(voucherRouter);
app.use(prizeHistoryRouter);
app.use(VoucherHistoryRouter)
app.use(diceRouter); // task NR4 

app.listen(port,()=>{
    console.log(`App đã chạy trên cổng : ${port}`)
})